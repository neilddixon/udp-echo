package com.neildixon.dev.udp.server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;

public class Server {

    public static void main(String[] args) throws IOException {

        String host = "localhost";
        int port = 0;

        if (args.length == 2) {
            host = args[0];
            port = Integer.parseInt(args[1]);
        } else {
            System.out.println("Parameters required: host port");
            System.exit(1);
        }

        DatagramSocket serverSocket = new DatagramSocket(new InetSocketAddress(host, port));
        System.out.println("Started server " + serverSocket.getLocalSocketAddress());

        try {
            while (true) {

                byte[] receivedData = new byte[1024];
                DatagramPacket receivePacket = new DatagramPacket(receivedData, receivedData.length);
                serverSocket.receive(receivePacket);
                System.out.println("Server received packet from: " + receivePacket.getSocketAddress());

                String data = new String(receivePacket.getData(), 0, receivePacket.getLength());
                InetAddress inetAddress = receivePacket.getAddress();
                int replyPort = receivePacket.getPort();
                String replyData = "OK, got:" + data;

                byte[] dataToSend = new byte[1024];
                dataToSend = replyData.getBytes();
                DatagramPacket sendPacket = new DatagramPacket(dataToSend, dataToSend.length, inetAddress, replyPort);
                System.out.println("Server sending reply to: " + sendPacket.getSocketAddress());
                serverSocket.send(sendPacket);
                System.out.println("Reply sent.");
            }
        } finally {
            if (serverSocket != null && !serverSocket.isClosed())
                serverSocket.close();
        }
    }
}
