package com.neildixon.dev.udp.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;

public class Client {

    public static void main(String[] args) throws IOException {

        String host = "localhost";
        int port = 0;
        String userMessage = "";

        if (args.length == 3) {
            host = args[0];
            port = Integer.parseInt(args[1]);
            userMessage = args[2];
        } else {
            System.out.println("Parameters required: host port message");
            System.exit(1);
        }


        DatagramSocket clientSocket = new DatagramSocket();
        try {
            InetAddress inetAddress = InetAddress.getByName(host);

            byte[] dataToSend = new byte[1024];
            dataToSend = userMessage.getBytes();

            DatagramPacket sendPacket = new DatagramPacket(dataToSend, dataToSend.length, inetAddress, port);
            System.out.println("Sending packet to " + sendPacket.getSocketAddress());
            Date packetSentAt = new Date();
            clientSocket.send(sendPacket);
            System.out.println("Packet sent. Awaiting response....");

            byte[] dataReceived = new byte[1024];
            DatagramPacket receivedPacket = new DatagramPacket(dataReceived, dataReceived.length);
            clientSocket.receive(receivedPacket);
            Date packetReceivedAt = new Date();
            String responseReceived = new String(receivedPacket.getData(), 0, receivedPacket.getLength());
            System.out.println("Server Replied with:\n" + responseReceived);

            long elapsedTimeMS = (packetReceivedAt.getTime() - packetSentAt.getTime());
            System.out.println("Roundtrip time(ms):" + elapsedTimeMS);

        }finally{
            if (clientSocket!=null && !clientSocket.isClosed())
                clientSocket.close();
        }

    }
}
